import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArtistDisplayerComponent } from './artist-displayer.component';

describe('ArtistDisplayerComponent', () => {
  let component: ArtistDisplayerComponent;
  let fixture: ComponentFixture<ArtistDisplayerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArtistDisplayerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArtistDisplayerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
