import { Component, OnInit } from '@angular/core';
import { DataDispenserService } from '../data-dispenser.service';
import { Observable, of,Subscription } from 'rxjs';

@Component({
  selector: 'app-artist-displayer',
  templateUrl: './artist-displayer.component.html',
  styleUrls: ['./artist-displayer.component.css']
})
export class ArtistDisplayerComponent implements OnInit {
	data: any ={};
	subscription: Subscription;


  //Registering the datadispenserService to auto update the displayer
  constructor(private dataDispenserService : DataDispenserService) {
  	this.subscription = this.dataDispenserService.getData().subscribe(data => {
			console.log(data);
			this.data= data;})
   }

  ngOnInit() {
  }
}
