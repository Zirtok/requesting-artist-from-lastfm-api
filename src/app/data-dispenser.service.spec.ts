import { TestBed } from '@angular/core/testing';

import { DataDispenserService } from './data-dispenser.service';

describe('DataDispenserService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DataDispenserService = TestBed.get(DataDispenserService);
    expect(service).toBeTruthy();
  });
});
