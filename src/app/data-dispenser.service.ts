import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of, Subject } from 'rxjs';



@Injectable({
	providedIn: 'root'
})
export class DataDispenserService {
	// using subject to update the displayer anytime data is changing
	private subject = new Subject<any>();
	data: any ={};
	apiUrl : string;
	constructor(private http: HttpClient) { }

	getDataFromServer() {
		return this.http.get(this.apiUrl);
	}

	getArtists() {
		this.getDataFromServer().subscribe(data => {
			//console.log(data);
			this.data= data;
			this.subject.next(this.data);
		})
	}

	requestData(apiUrl: string)
	{
		this.apiUrl = apiUrl;
		this.getArtists();
		this.getDataFromServer();
	}

	//setting subject as the object to observe
	getData(): Observable<any>
	{
		return this.subject.asObservable();;
	}

	//clearing data when noting is requested
	clearData()
	{
		this.data = {};
		this.subject.next(this.data);
	}




}
