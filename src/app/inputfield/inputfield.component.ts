import { Component, OnInit } from '@angular/core';
import { DataDispenserService } from '../data-dispenser.service';
import { FormsModule } from '@angular/forms';

@Component({
	selector: 'app-inputfield',
	templateUrl: './inputfield.component.html',
	styleUrls: ['./inputfield.component.css']
})
export class InputfieldComponent implements OnInit {
	artistRequested : string;
	message : string
	apiUrl :  string ;
	apiUrlOrigin : string;


	// initializing properties & registering the DataDispenserService to send requests
	constructor(private dataDispenserService : DataDispenserService) {
		this.artistRequested = '';
		this.apiUrlOrigin = "http://ws.audioscrobbler.com/2.0/?method=artist.search&api_key=%209e0eeb50a3c6c02d3fd0860f545e6b2f&format=json&limit=50&artist=";
	}

	ngOnInit() {
	}

	// used when the user click on the search button
	// update the url and forward it to the DataDispenserService
	sendRequest()
	{
		if (this.artistRequested.length == 0)
		{
			this.message = 'I cant read any artist';
			this.dataDispenserService.clearData();
		} 
		else
		{
		this.apiUrl = this.apiUrlOrigin + this.artistRequested;
		this.dataDispenserService.requestData(this.apiUrl);
	}
	} 





}
